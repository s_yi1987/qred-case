module.exports = {
    jsxBracketSameLine: false,
    singleQuote: true,
    trailingComma: 'all',
    tabWidth: 4,
    semi: true,
    arrowParens: 'always',
};
