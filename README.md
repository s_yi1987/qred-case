# README

Sample camera app made using React Native and various other libraries. Showcases how to use dependency injections with the factory pattern to maximize testability.

### How do I get set up?

-   yarn install
-   cd ios && pod install (if running on Mac)
-   yarn android or yarn ios
