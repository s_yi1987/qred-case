export default class Amount {
    constructor({ number, currencyCode }) {
        this.number = number;
        this.currencyCode = currencyCode;
    }
}

const amountToString = (amount) => `${amount.number} ${amount.currencyCode}`;

export { amountToString };
