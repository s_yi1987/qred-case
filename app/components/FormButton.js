import React from 'react';
import {
    TouchableOpacity,
    Text,
    StyleSheet,
    ViewPropTypes,
} from 'react-native';
import PropTypes from 'prop-types';

import colors from '../assets/colors';

const FormButton = (props) => {
    const { label, style, onPress } = props;

    return (
        <TouchableOpacity style={[styles.button, style]} onPress={onPress}>
            <Text style={styles.label}>{label}</Text>
        </TouchableOpacity>
    );
};

FormButton.propTypes = {
    label: PropTypes.string.isRequired,
    style: ViewPropTypes.style,
    onPress: PropTypes.func,
};

FormButton.defaultProps = {
    style: undefined,
    onPress: undefined,
};

const styles = StyleSheet.create({
    button: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.primaryBrand,
    },
    label: {
        color: 'white',
        fontWeight: 'bold',
    },
});

export default FormButton;
