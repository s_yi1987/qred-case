import React, { useContext } from 'react';

import ApiLayerContext from '../../contexts/ApiLayerContext';

export default (CameraScene) => (props) => {
    const api = useContext(ApiLayerContext);

    return (
        <CameraScene
            {...props}
            extractPaymentDetails={api.extractPaymentDetails}
        />
    );
};
