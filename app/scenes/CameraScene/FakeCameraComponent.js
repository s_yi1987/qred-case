import React, { useImperativeHandle, forwardRef, useCallback } from 'react';
import { View, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';

const FakeCameraComponent = forwardRef((props, ref) => {
    const { style } = props;

    const takePictureAsync = useCallback(
        (options) => {
            const result = { uri: 'someFakeUri' };
            if (options.base64) {
                result.base64 = 'someFakeBase64';
            }
            return new Promise((resolve) => {
                setTimeout(() => {
                    resolve(result);
                }, 3000);
            });
        },
        [props],
    );

    useImperativeHandle(ref, () => ({ takePictureAsync }), []);

    return <View style={style} />;
});

FakeCameraComponent.propTypes = {
    style: PropTypes.oneOfType([ViewPropTypes.style, PropTypes.array]),
};

FakeCameraComponent.defaultProps = {
    style: undefined,
};

export default FakeCameraComponent;
