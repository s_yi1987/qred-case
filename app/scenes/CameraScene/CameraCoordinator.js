import React, { useCallback } from 'react';

export default (CameraScene) => (props) => {
    const { navigation, ...rest } = props;

    const onPaymentDetailsRetrieved = useCallback(
        (paymentDetails) => {
            navigation.navigate('PaymentDetailsScene', { paymentDetails });
        },
        [navigation],
    );

    return (
        <CameraScene
            {...rest}
            onPaymentDetailsRetrieved={onPaymentDetailsRetrieved}
        />
    );
};
