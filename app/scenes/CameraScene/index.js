import React, { useRef, useCallback, useState } from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    Modal,
    Alert,
} from 'react-native';
import PropTypes from 'prop-types';

import colors from '../../assets/colors';
import AppError from '../../errors/AppError';

const CameraScene = (props) => {
    const {
        CameraComponent,
        extractPaymentDetails,
        onPaymentDetailsRetrieved,
    } = props;
    const cameraRef = useRef();

    const [loading, setLoading] = useState(false);

    const onCameraButtonPress = useCallback(async () => {
        // TODO: Put debounce

        try {
            setLoading(true);
            const res = await cameraRef.current.takePictureAsync({
                base64: true,
            });

            const paymentDetails = await extractPaymentDetails({
                imageBase64: res.base64,
            });

            setLoading(false);
            onPaymentDetailsRetrieved(paymentDetails);
        } catch (error) {
            setLoading(false);
            if (error instanceof AppError) {
                const dialogData = error.getDialogData();
                Alert.alert(dialogData.title, dialogData.message);
            }
        }
    }, [extractPaymentDetails, onPaymentDetailsRetrieved, CameraComponent]);

    const renderLoadingIndicatorIfNeeded = () => {
        return (
            <Modal visible={loading} transparent>
                <View style={styles.modalContainer}>
                    <ActivityIndicator
                        size="large"
                        color={colors.primaryBrand}
                    />
                </View>
            </Modal>
        );
    };

    return (
        <View style={styles.root}>
            <CameraComponent ref={cameraRef} style={styles.camera} />
            <View style={styles.cameraBottomSection}>
                <TouchableOpacity
                    accessibilityLabel="Take picture"
                    style={styles.cameraButton}
                    onPress={onCameraButtonPress}
                />
            </View>
            {renderLoadingIndicatorIfNeeded()}
        </View>
    );
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
    },
    camera: {
        flex: 1,
        backgroundColor: 'black',
    },
    cameraBottomSection: {
        height: 100,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
    },
    cameraButton: {
        width: 50,
        height: 50,
        borderRadius: 50 / 2,
        backgroundColor: 'white',
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'clear',
    },
});

CameraScene.propTypes = {
    CameraComponent: PropTypes.elementType.isRequired,
    extractPaymentDetails: PropTypes.func.isRequired,
    onPaymentDetailsRetrieved: PropTypes.func,
};

CameraScene.defaultProps = {
    onPaymentDetailsRetrieved: () => {},
};

export default CameraScene;
