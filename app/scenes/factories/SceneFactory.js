import React from 'react';
import { RNCamera } from 'react-native-camera';

import CameraScene from '../CameraScene';
import connectApiToCameraScene from '../CameraScene/apiConnector';
import FakeCameraComponent from '../CameraScene/FakeCameraComponent';

import PaymentDetailsScene from '../PaymentDetailsScene';

import PaymentSelectScene from '../PaymentSelectScene';
import connectApiToPaymentSelectScene from '../PaymentSelectScene/apiConnector';

import SigningScene from '../SigningScene';

export default class SceneFactory {
    cameraScene = () => {
        // Haven't been able to test integration with the real camera component
        // because of M1 compatibility issues on React Native and since I don't have
        // a real android device to test it on but it should work
        const useFake = true;
        return connectApiToCameraScene((props) => (
            <CameraScene
                {...props}
                CameraComponent={useFake ? FakeCameraComponent : RNCamera}
            />
        ));
    };

    paymentDetailsScene = () => {
        return PaymentDetailsScene;
    };

    paymentSelectScene = () => {
        return connectApiToPaymentSelectScene(PaymentSelectScene);
    };

    signingScene = () => {
        return SigningScene;
    };
}
