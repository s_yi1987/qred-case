import React from 'react';
import { View, SafeAreaView, StyleSheet } from 'react-native';

import FormButton from '../../components/FormButton';

const SigningScene = () => {
    return (
        <SafeAreaView style={styles.root}>
            <View style={styles.root} />
            <FormButton style={styles.button} label="CONFIRM" />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        backgroundColor: 'white',
    },
    button: {
        marginHorizontal: 12,
        marginBottom: 20,
    },
});

SigningScene.propTypes = {};

SigningScene.defaultProps = {};

export default SigningScene;
