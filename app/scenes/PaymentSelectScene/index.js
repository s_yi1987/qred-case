import React, { useCallback, useState, useEffect } from 'react';
import { View, StyleSheet, ActivityIndicator, Alert } from 'react-native';
import PropTypes from 'prop-types';

import PaymentSelectView from './PaymentSelectView';
import colors from '../../assets/colors';
import Amount from '../../models/Amount';
import AppError from '../../errors/AppError';

const PaymentSelectScene = (props) => {
    const { onPaymentSelect, fetchPaymentOptions, paymentDetails } = props;

    const [selectedIndex, setSelectedIndex] = useState(0);
    const [loading, setLoading] = useState(false);
    const [paymentOptions, setPaymentOptions] = useState();

    const onPaymentTypeSelect = useCallback((index) => {
        setSelectedIndex(index);
    }, []);

    const onNextButtonPress = useCallback(() => {
        onPaymentSelect(selectedIndex);
    }, [onPaymentSelect]);

    useEffect(() => {
        const componentDidMount = async () => {
            try {
                setLoading(true);
                const newPaymentOptions = await fetchPaymentOptions({
                    amount: paymentDetails.amount,
                });
                setPaymentOptions(newPaymentOptions);
                setLoading(false);
            } catch (error) {
                setLoading(false);
                if (error instanceof AppError) {
                    const dialogData = error.getDialogData();
                    Alert.alert(dialogData.title, dialogData.message);
                }
            }
        };
        componentDidMount();
    }, []);

    const renderLoadingIndicatorIfNeeded = () => {
        if (!loading) {
            return null;
        }
        return (
            <View style={styles.modalContainer}>
                <ActivityIndicator size="large" color={colors.primaryBrand} />
            </View>
        );
    };

    const renderPaymentSelectView = () => {
        if (!paymentOptions) {
            return null;
        }
        return (
            <PaymentSelectView
                onPaymentTypeSelect={onPaymentTypeSelect}
                onNextButtonPress={onNextButtonPress}
                paymentOptions={paymentOptions}
            />
        );
    };

    return (
        <View style={styles.root}>
            {renderPaymentSelectView()}
            {renderLoadingIndicatorIfNeeded()}
        </View>
    );
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        backgroundColor: 'white',
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'clear',
    },
});

PaymentSelectScene.propTypes = {
    paymentDetails: PropTypes.shape({
        receiver: PropTypes.string,
        amount: PropTypes.instanceOf(Amount),
        dueDate: PropTypes.number,
    }).isRequired,
    onPaymentSelect: PropTypes.func,
    fetchPaymentOptions: PropTypes.func.isRequired,
};

PaymentSelectScene.defaultProps = {
    onPaymentSelect: () => {},
};

export default PaymentSelectScene;
