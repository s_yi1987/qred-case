import React, { useCallback, useMemo } from 'react';

export default (PaymentSelectScene) => (props) => {
    const { navigation, route, ...rest } = props;

    const onPaymentSelect = useCallback(() => {
        navigation.navigate('SigningScene');
    }, [navigation]);

    const paymentDetails = useMemo(() => {
        if (route && route.params) {
            return route.params.paymentDetails;
        }
        return null;
    }, [route]);

    return (
        <PaymentSelectScene
            {...rest}
            paymentDetails={paymentDetails}
            onPaymentSelect={onPaymentSelect}
        />
    );
};
