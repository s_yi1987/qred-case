import React, { useContext, useCallback } from 'react';

import ApiLayerContext from '../../contexts/ApiLayerContext';
import { amountToString } from '../../models/Amount';

export default (PaymentSelectScene) => (props) => {
    const api = useContext(ApiLayerContext);
    const recognizedKeys = [
        'optionPayNow',
        'optionPayIn30Days',
        'optionSplit12Months',
    ];
    const additionalMetadata = {
        optionPayNow: {
            label: 'Pay now',
        },
        optionPayIn30Days: {
            label: 'Pay in 30 days',
        },
        optionSplit12Months: {
            label: 'Split in 12 months',
        },
    };

    const fetchPaymentOptions = useCallback(async ({ amount }) => {
        const paymentOptions = await api.fetchPaymentOptions({ amount });

        const adaptedPaymentOptions = [];
        for (let i = 0; i < recognizedKeys.length; i += 1) {
            const key = recognizedKeys[i];
            const paymentOption = paymentOptions[key];

            if (paymentOption) {
                const amountStr = amountToString(
                    paymentOption.amountSplitUp || paymentOption.amount,
                );

                adaptedPaymentOptions.push({
                    id: key,
                    ...paymentOption,
                    ...additionalMetadata[key],
                    label: `${additionalMetadata[key].label}\n${amountStr}`,
                });
            }
        }

        return adaptedPaymentOptions;
    }, []);

    return (
        <PaymentSelectScene
            {...props}
            fetchPaymentOptions={fetchPaymentOptions}
        />
    );
};
