import React, { useMemo, useCallback } from 'react';
import { View, SafeAreaView, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import RadioGroup from 'react-native-radio-buttons-group';

import FormButton from '../../components/FormButton';

const PaymentSelectView = (props) => {
    const { onPaymentTypeSelect, onNextButtonPress, paymentOptions } = props;
    const onRadioGroupPress = useCallback(
        (radioButtons) => {
            let selectedIndex = 0;
            for (let i = 0; i < radioButtons.length; i += 1) {
                const radioButton = radioButtons[i];
                if (radioButton.selected) {
                    selectedIndex = i;
                    break;
                }
            }
            onPaymentTypeSelect(selectedIndex);
        },
        [onPaymentTypeSelect],
    );

    const radioButtons = useMemo(() => {
        return paymentOptions.map((paymentOption, index) => {
            return {
                ...paymentOption,
                selected: index === 0,
                containerStyle: styles.radioButton,
            };
        });
    }, [paymentOptions]);

    return (
        <SafeAreaView style={styles.root}>
            <RadioGroup
                containerStyle={styles.radioButtonGroup}
                radioButtons={radioButtons}
                onPress={onRadioGroupPress}
            />
            <View style={styles.root} />
            <FormButton
                style={styles.button}
                label="NEXT"
                onPress={onNextButtonPress}
            />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        backgroundColor: 'white',
    },
    button: {
        marginHorizontal: 12,
        marginBottom: 20,
    },
    radioButtonGroup: {
        marginTop: 20,
        marginHorizontal: 20,
        alignItems: 'stretch',
    },
    radioButton: {
        marginTop: 20,
    },
});

PaymentSelectView.propTypes = {
    onPaymentTypeSelect: PropTypes.func,
    onNextButtonPress: PropTypes.func,
    paymentOptions: PropTypes.array.isRequired,
};

PaymentSelectView.defaultProps = {
    onPaymentTypeSelect: () => {},
    onNextButtonPress: undefined,
};

export default PaymentSelectView;
