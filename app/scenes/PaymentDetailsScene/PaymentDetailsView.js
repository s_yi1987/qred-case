import React, { useState, useCallback } from 'react';
import {
    View,
    StyleSheet,
    SafeAreaView,
    TextInput,
    Text,
    TouchableOpacity,
} from 'react-native';
import DatePicker from 'react-native-date-picker';
import PropTypes from 'prop-types';
import { format } from 'date-fns';

import FormButton from '../../components/FormButton';
import Amount from '../../models/Amount';

const PaymentDetailsView = (props) => {
    const { receiver, amount, dueDate, onChangeDueDate, onNextButtonPress } =
        props;
    const amountTextTitle = amount
        ? `Amount (${amount.currencyCode})`
        : 'Amount';

    const [showDatePicker, setShowDatePicker] = useState(false);

    const onDatePickerConfirm = useCallback((newDate) => {
        setShowDatePicker(false);
        onChangeDueDate(newDate.getTime());
    }, []);

    const onDatePickerCancel = useCallback(() => {
        setShowDatePicker(false);
    }, []);

    const onDueDateButtonPress = useCallback(() => {
        setShowDatePicker(true);
    }, []);

    const onButtonPress = useCallback(() => {
        onNextButtonPress();
    }, [onNextButtonPress]);

    return (
        <SafeAreaView style={styles.root}>
            <View style={styles.root}>
                <Text style={styles.text}>Receiver</Text>
                <TextInput style={styles.input} value={receiver} />
                <Text style={styles.text}>{amountTextTitle}</Text>
                <TextInput
                    style={styles.input}
                    value={amount ? amount.number.toString() : undefined}
                    keyboardType="numeric"
                />
                <Text style={styles.text}>Due date</Text>
                <TouchableOpacity
                    style={styles.input}
                    onPress={onDueDateButtonPress}
                >
                    <Text style={styles.dueDateText}>
                        {format(dueDate, 'yyyy-MM-dd')}
                    </Text>
                </TouchableOpacity>
                <View style={styles.root} />
                <FormButton
                    label="NEXT"
                    style={styles.button}
                    onPress={onButtonPress}
                />
            </View>
            <DatePicker
                modal
                open={showDatePicker}
                date={new Date(dueDate)}
                onConfirm={onDatePickerConfirm}
                onCancel={onDatePickerCancel}
                mode="date"
            />
        </SafeAreaView>
    );
};

PaymentDetailsView.propTypes = {
    receiver: PropTypes.string,
    amount: PropTypes.instanceOf(Amount),
    dueDate: PropTypes.number,
    onChangeDueDate: PropTypes.func,
    onNextButtonPress: PropTypes.func,
};

PaymentDetailsView.defaultProps = {
    receiver: undefined,
    amount: undefined,
    dueDate: undefined,
    onChangeDueDate: () => {},
    onNextButtonPress: () => {},
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        backgroundColor: 'white',
    },
    button: {
        marginHorizontal: 12,
        marginBottom: 20,
    },
    input: {
        height: 40,
        marginTop: 8,
        marginHorizontal: 12,
        borderWidth: 1,
        padding: 10,
    },
    dueDateText: {
        color: 'black',
    },
    text: {
        marginTop: 22,
        marginLeft: 12,
    },
});

export default PaymentDetailsView;
