import React, { useState, useCallback } from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import PaymentDetailsView from './PaymentDetailsView';
import Amount from '../../models/Amount';

const PaymentDetailsScene = (props) => {
    const { initialPaymentDetails, onVerify } = props;

    const [receiver, setReceiver] = useState(initialPaymentDetails.receiver);
    const [amount, setAmount] = useState(initialPaymentDetails.amount);
    const [dueDate, setDueDate] = useState(initialPaymentDetails.dueDate);

    const onChangeReceiver = useCallback((newReceiver) => {
        setReceiver(newReceiver);
    }, []);

    const onChangeAmount = useCallback((newAmount) => {
        setAmount(newAmount);
    }, []);

    const onChangeDueDate = useCallback((newDate) => {
        setDueDate(newDate);
    }, []);

    const onNextButtonPress = useCallback(() => {
        onVerify({
            receiver,
            amount,
            dueDate,
        });
    }, []);

    return (
        <View style={styles.root}>
            <PaymentDetailsView
                style={styles.root}
                receiver={receiver}
                onChangeReceiver={onChangeReceiver}
                amount={amount}
                onChangeAmount={onChangeAmount}
                dueDate={dueDate}
                onChangeDueDate={onChangeDueDate}
                onNextButtonPress={onNextButtonPress}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
    },
});

PaymentDetailsScene.propTypes = {
    initialPaymentDetails: PropTypes.shape({
        receiver: PropTypes.string,
        amount: PropTypes.instanceOf(Amount),
        dueDate: PropTypes.number,
    }).isRequired,
    onVerify: PropTypes.func,
};

PaymentDetailsScene.defaultProps = {
    onVerify: () => {},
};

export default PaymentDetailsScene;
