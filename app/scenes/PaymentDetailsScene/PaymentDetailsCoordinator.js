import React, { useCallback, useMemo } from 'react';

export default (PaymentDetailsScene) => (props) => {
    const { navigation, route, ...rest } = props;

    const onVerify = useCallback(
        (paymentDetails) => {
            navigation.navigate('PaymentSelectScene', { paymentDetails });
        },
        [navigation],
    );

    const initialPaymentDetails = useMemo(() => {
        if (route && route.params) {
            return route.params.paymentDetails;
        }
        return null;
    }, [route]);

    return (
        <PaymentDetailsScene
            {...rest}
            initialPaymentDetails={initialPaymentDetails}
            onVerify={onVerify}
        />
    );
};
