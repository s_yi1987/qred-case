import React, { useMemo, useContext } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import SceneFactoryContext from '../../contexts/SceneFactoryContext';
import coordinateCameraScene from '../CameraScene/CameraCoordinator';
import coordinatePaymentDetailsScene from '../PaymentDetailsScene/PaymentDetailsCoordinator';
import coordinatePaymentSelectScene from '../PaymentSelectScene/PaymentSelectCoordinator';

const Stack = createNativeStackNavigator();

const RootStackNavigator = () => {
    const sceneFactory = useContext(SceneFactoryContext);

    const CameraScene = useMemo(
        () => coordinateCameraScene(sceneFactory.cameraScene()),
        [sceneFactory],
    );

    const PaymentDetailsScene = useMemo(
        () => coordinatePaymentDetailsScene(sceneFactory.paymentDetailsScene()),
        [sceneFactory],
    );

    const PaymentSelectScene = useMemo(
        () => coordinatePaymentSelectScene(sceneFactory.paymentSelectScene()),
        [sceneFactory],
    );

    const SigningScene = useMemo(
        () => sceneFactory.signingScene(),
        [sceneFactory],
    );

    return (
        <Stack.Navigator mode="modal" initialRouteName="CameraScene">
            <Stack.Screen
                name="CameraScene"
                component={CameraScene}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="PaymentDetailsScene"
                component={PaymentDetailsScene}
                options={{ title: 'Invoice' }}
            />
            <Stack.Screen
                name="PaymentSelectScene"
                component={PaymentSelectScene}
                options={{ title: 'Select Payment' }}
            />
            <Stack.Screen
                name="SigningScene"
                component={SigningScene}
                options={{ title: 'Sign & Seal' }}
            />
        </Stack.Navigator>
    );
};

export default RootStackNavigator;
