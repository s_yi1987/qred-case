import AppError from './AppError';

export default class NetworkError extends AppError {
    constructor(errorResponse) {
        super();
        this.errorResponse = errorResponse;
    }
}
