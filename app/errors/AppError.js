export default class AppError extends Error {
    getDialogData = () => {
        return {
            title: 'Oops!',
            message: 'Something unexpected happened. Please try again later.',
        };
    };

    getAnalyticsData = () => {
        return {};
    };
}
