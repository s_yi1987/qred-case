import AppError from './AppError';

export default class NoResponseError extends AppError {
    constructor(failedRequest) {
        super();
        this.failedRequest = failedRequest;
    }
}
