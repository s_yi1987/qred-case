import Amount from '../models/Amount';

export default class FakeApiLayer {
    extractPaymentDetails = () => {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve({
                    receiver: 'Tele2 AB',
                    amount: new Amount({ number: 19157, currencyCode: 'SEK' }),
                    dueDate: Date.now(),
                });
            }, 3000);
        });
    };

    fetchPaymentOptions = ({ amount }) => {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve({
                    optionPayNow: {
                        amount,
                    },
                    optionPayIn30Days: {
                        amount: new Amount({
                            number: Math.round(amount.number * 1.015),
                            currencyCode: amount.currencyCode,
                        }),
                    },
                    optionSplit12Months: {
                        amount: new Amount({
                            number: amount.number * 1.015,
                            currencyCode: amount.currencyCode,
                        }),
                        amountSplitUp: new Amount({
                            number: Math.round((amount.number * 1.015) / 12),
                            currencyCode: amount.currencyCode,
                        }),
                    },
                });
            }, 1000);
        });
    };
}
