import NoResponseError from '../errors/NoResponseError';

export default class AppApiLayer {
    constructor({ qredNetworkLayer }) {
        this.qredNetworkLayer = qredNetworkLayer;
    }

    extractPaymentDetails = async ({ imageBase64 }) => {
        const result = await this.qredNetworkLayer.request({
            method: 'post',
            url: '/CaseStudy_Dummy_Endpoint',
            data: {
                image: `data:image/jpeg;base64,${imageBase64}`,
            },
        });

        // Forcefully throwing an error here because the above endpoint actually works but returns some dummy string
        throw new NoResponseError(result.request);
        // return result.data || {};
    };

    fetchPaymentOptions = async ({ amount }) => {
        const result = await this.qredNetworkLayer.request({
            method: 'get',
            url: `/CaseStudy_Dummy_Endpoint2?amount=${amount.number}&currency_code=${amount.currencyCode}`,
        });

        return result.data || {};
    };
}
