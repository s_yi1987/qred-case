import NetworkError from '../errors/NetworkError';
import NoResponseError from '../errors/NoResponseError';

export default class NetworkErrorHandler {
    constructor({ networkLayer }) {
        this.networkLayer = networkLayer;
    }

    async request(options) {
        try {
            const result = await this.networkLayer.request(options);
            return result;
        } catch (error) {
            if (error.response) {
                throw new NetworkError(error.response);
            } else if (error.request) {
                throw new NoResponseError(error.request);
            } else {
                throw new Error(error.message);
            }
        }
    }
}
