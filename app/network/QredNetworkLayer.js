import axios from 'axios';

export default class QredNetworkLayer {
    constructor() {
        this.axiosInstance = axios.create({
            baseURL:
                'https://c20zuamcv7.execute-api.eu-north-1.amazonaws.com/default',
        });
    }

    request(options) {
        return this.axiosInstance.request(options);
    }
}
