import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';

import RootNavigator from './scenes/RootNavigator';
import FakeApiLayer from './api/FakeApiLayer';
import AppApiLayer from './api/AppApiLayer';

import SceneFactory from './scenes/factories/SceneFactory';
import SceneFactoryContext from './contexts/SceneFactoryContext';

import ApiLayerContext from './contexts/ApiLayerContext';
import QredNetworkLayer from './network/QredNetworkLayer';
import NetworkErrorHandler from './network/NetworkErrorHandler';

const App = () => {
    const [apiLayer] = useState(new FakeApiLayer());
    // eslint-disable-next-line no-unused-vars
    const [appApiLayer] = useState(
        // This is unused but can be freely switched in place of the FakeApiLayer
        // will error out however since there's no backend functionality for it
        new AppApiLayer({
            qredNetworkLayer: new NetworkErrorHandler({
                networkLayer: new QredNetworkLayer(),
            }),
        }),
    );
    const [factory] = useState(new SceneFactory());

    return (
        <SceneFactoryContext.Provider value={factory}>
            <ApiLayerContext.Provider value={apiLayer}>
                <NavigationContainer>
                    <RootNavigator />
                </NavigationContainer>
            </ApiLayerContext.Provider>
        </SceneFactoryContext.Provider>
    );
};

export default App;
