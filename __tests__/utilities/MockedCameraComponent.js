import React, { forwardRef, useCallback, useImperativeHandle } from 'react';
import { View } from 'react-native';

const MockedCameraComponent = forwardRef((props, ref) => {
    const takePictureAsync = useCallback(() => {
        const result = { uri: 'someFakeUri' };
        result.base64 = 'someFakeBase64';

        return Promise.resolve(result);
    }, [props]);

    useImperativeHandle(ref, () => ({ takePictureAsync }), []);
    return <View />;
});

export default MockedCameraComponent;
