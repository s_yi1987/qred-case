import React from 'react';
import { View } from 'react-native';
import { render, waitFor, fireEvent } from '@testing-library/react-native';
import { NavigationContainer } from '@react-navigation/native';

import CameraScene from '../../app/scenes/CameraScene';
import Amount from '../../app/models/Amount';
import RootNavigator from '../../app/scenes/RootNavigator';
import SceneFactoryContext from '../../app/contexts/SceneFactoryContext';
import MockedCameraComponent from '../utilities/MockedCameraComponent';

class MockedSceneFactory {
    cameraScene = () => {
        const dueDate = Date.now();
        const extractPaymentDetails = () => {
            return Promise.resolve({
                receiver: 'Test',
                amount: new Amount({ number: 1337, currencyCode: 'SEK' }),
                dueDate,
            });
        };
        return (props) => (
            <CameraScene
                {...props}
                CameraComponent={MockedCameraComponent}
                extractPaymentDetails={extractPaymentDetails}
            />
        );
    };

    paymentDetailsScene = () => {
        return () => {
            return <View accessibilityLabel="paymentDetailsScene" />;
        };
    };

    paymentSelectScene = () => {
        return View;
    };

    signingScene = () => {
        return View;
    };
}

describe('Happy case', () => {
    it('navigate correctly to the various scenes', async () => {
        const { getByA11yLabel } = render(
            <SceneFactoryContext.Provider value={new MockedSceneFactory()}>
                <NavigationContainer>
                    <RootNavigator />
                </NavigationContainer>
            </SceneFactoryContext.Provider>,
        );

        await waitFor(() => getByA11yLabel('Take picture'));
        fireEvent.press(getByA11yLabel('Take picture'));

        await waitFor(() => getByA11yLabel('paymentDetailsScene'));
    });
});
