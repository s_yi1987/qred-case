import React from 'react';
import { render, waitFor, fireEvent } from '@testing-library/react-native';

import CameraScene from '../../app/scenes/CameraScene';
import Amount from '../../app/models/Amount';
import MockedCameraComponent from '../utilities/MockedCameraComponent';

describe('CameraScene', () => {
    describe('onPaymentDetailsRetrieved', () => {
        it('should be called properly with the right parameters', async () => {
            const dueDate = Date.now();
            const extractPaymentDetails = () => {
                return Promise.resolve({
                    receiver: 'Test',
                    amount: new Amount({ number: 19157, currencyCode: 'SEK' }),
                    dueDate,
                });
            };

            const onPaymentDetailsRetrieved = jest.fn();

            const { getByA11yLabel, update } = render(
                <CameraScene
                    extractPaymentDetails={extractPaymentDetails}
                    CameraComponent={MockedCameraComponent}
                    onPaymentDetailsRetrieved={onPaymentDetailsRetrieved}
                />,
            );

            await waitFor(() => getByA11yLabel('Take picture'));
            fireEvent.press(getByA11yLabel('Take picture'));

            await waitFor(() =>
                expect(onPaymentDetailsRetrieved).toHaveBeenCalled(),
            );
            expect(onPaymentDetailsRetrieved.mock.calls.length).toBe(1);
            expect(onPaymentDetailsRetrieved.mock.calls[0][0].receiver).toBe(
                'Test',
            );
            expect(
                onPaymentDetailsRetrieved.mock.calls[0][0].amount.number,
            ).toBe(19157);
            expect(onPaymentDetailsRetrieved.mock.calls[0][0].dueDate).toBe(
                dueDate,
            );

            const extractPaymentDetails2 = () => {
                return Promise.resolve({
                    receiver: 'Test2',
                    amount: new Amount({ number: 222, currencyCode: 'SEK' }),
                    dueDate,
                });
            };

            update(
                <CameraScene
                    extractPaymentDetails={extractPaymentDetails2}
                    CameraComponent={MockedCameraComponent}
                    onPaymentDetailsRetrieved={onPaymentDetailsRetrieved}
                />,
            );

            await waitFor(() => getByA11yLabel('Take picture'));
            fireEvent.press(getByA11yLabel('Take picture'));

            await waitFor(() =>
                expect(onPaymentDetailsRetrieved).toHaveBeenCalled(),
            );
            expect(onPaymentDetailsRetrieved.mock.calls.length).toBe(2);
            expect(onPaymentDetailsRetrieved.mock.calls[1][0].receiver).toBe(
                'Test2',
            );
            expect(
                onPaymentDetailsRetrieved.mock.calls[1][0].amount.number,
            ).toBe(222);
        });
    });
});
