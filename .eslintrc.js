module.exports = {
    env: {
        jest: true,
    },
    extends: [
        'airbnb',
        'prettier',
        'prettier/react',
        'plugin:prettier/recommended',
        'eslint-config-prettier',
    ],
    parser: 'babel-eslint',
    rules: {
        'import/no-unresolved': 'off',
        'react/jsx-props-no-spreading': 'off',
        'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
        'react/forbid-prop-types': 'off',
        'prettier/prettier': ['error'],
        'no-use-before-define': 'off',
        'react/prop-types': [
            'error',
            {
                ignore: ['navigation', 'api', 'route'],
            },
        ],
    },
    plugins: ['prettier'],
};
